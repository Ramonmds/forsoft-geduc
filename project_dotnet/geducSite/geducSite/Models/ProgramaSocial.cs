﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace geducSite.Models
{
    public class ProgramaSocial
    {
        public int idProgramaSocial { get; set; }
        public string nomePrograma { get; set; }
        public string descricao { get; set; }
        public string ambitoAdm { get; set; }
    }
}