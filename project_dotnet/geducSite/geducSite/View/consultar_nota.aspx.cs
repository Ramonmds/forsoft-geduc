﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class consultar_nota : System.Web.UI.Page
    {
        protected IEnumerable<Nota> todasAsNotas;
        protected IEnumerable<Nota> retorno;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["todasAsNotas"] != null)
            {
                todasAsNotas = (IEnumerable<Nota>)Session["todasAsNotas"];
            }
            else
            {
                todasAsNotas = new NotaDAO().Listar();
                Session["todasAsNotas"] = todasAsNotas;
                Session.Timeout = 6000;
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            BuscarNotas();
        }

        protected IEnumerable<Nota> BuscarNotas()
        {
            int valor = Convert.ToInt32(ddlTipoDeBusca.SelectedValue);
            if (txtBuscar.Text.Length != 0)
            {
                switch (valor)
                {
                    case 1:
                        retorno = todasAsNotas.Where(x => x.aluno.matricula.ToUpper().Contains(txtBuscar.Text.ToUpper()));
                        if (retorno.Count() > 0)
                            DivBusca.Visible = true;
                        break;
                    case 2:
                        retorno = todasAsNotas.Where(x => x.nota == Convert.ToInt32(txtBuscar.Text));
                        if (retorno.Count() > 0)
                            DivBusca.Visible = true;
                        break;
                    case 3:
                        retorno = todasAsNotas.Where(x => x.disciplina.nome.ToUpper().Contains(txtBuscar.Text.ToUpper()));
                        if (retorno.Count() > 0)
                            DivBusca.Visible = true;
                        break;
                    case 4:
                        retorno = todasAsNotas.Where(x => Convert.ToString(x.data).ToUpper().Contains(txtBuscar.Text.ToUpper()));
                        DivBusca.Visible = true;
                        break;
                }
            }
            return retorno;
        }


    }
}
